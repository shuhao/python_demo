import pickle


# 创建文本文档
def createFile():
    myfile = open('myfile.txt', 'w')
    myfile.write('hello text file\n')
    myfile.write('have a good day\n')
    myfile.close()


# 使用for循环迭代读取文本
def readFile():
    myfile = open('myfile.txt', 'r')
    for line in myfile:
        print(line, end=',')


# 读取文件一行，以空字符""结尾表示没有文本
def readLine():
    myfile = open('myfile.txt', 'r')
    i = 0
    while True:
        # 循环到哪一行，将该行的文件读入到内存
        line = myfile.readline()
        if line == "":
            break
        i = i + 1
        print(line)
        pass
    print("共有%d行" % i)


def readLine2():
    myfile = open('myfile.txt', 'r')
    # 一次将文件全部读入到内存
    for line in myfile.readlines():
        print(line, end="")
    pass


# 创建二进制文件 open文件第二个参数表示对文件的操作方式，b表示二进制文件
def createByteFile():
    byte_file = open("data.bin", "wb")
    byte_file.write(b"abcdefg")
    byte_file.close()
    pass


# 读取二进制文件
def readByteFile():
    byte_file = open("data.bin", "rb")
    data = byte_file.read()
    print(data)
    print(list(data))
    byte_file.close()
    pass


##对象序列化模块pickle
# 对象存储
def saveObject():
    D = {'a': 1, 'b': 2, 'c': 3}
    L = [3, 4, 5]
    with open('datafile.pkl', 'wb') as file:
        pickle.dump(D, file)
        pickle.dump(L, file)
        pass
    pass


# 从文件中读取对象存储
def readObject():
    with open('datafile.pkl', 'rb') as file:
        print(pickle.load(file))
        print(pickle.load(file))
        pass
    pass


if __name__ == "__main__":
    createFile()
    readLine()
    pass
