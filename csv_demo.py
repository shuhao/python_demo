import csv
from collections import namedtuple

path = "./data.csv"


def readToList():
    with open(path) as f:
        f_csv = csv.reader(f)
        for row in f_csv:
            print("Symbol is :", row[0])
            print("Price is :", row[1])
            print("Date is :", row[2])
            print("Time is :", row[3])
            print("Symbol is :", row[4])
            print("Volume is :", row[5])
            print("\n")


def readToTuple():
    with open(path) as f:
        f_csv = csv.reader(f)
        headings = next(f_csv)
        Row = namedtuple('Row', headings)
        for row in f_csv:
            r = Row(*row)
            print(r)
            print(r.Symbol)


def readToDict():
    with open(path) as f:
        f_csv = csv.DictReader(f)
        for row in f_csv:
            print(type(row))
            print(row['Symbol'])


if __name__ == "__main__":
    readToDict()
    pass
