# 派生内置不可变类型，并且修改实例化行为

# 新型元组，无论实例数据时什么，仅保存大于0的int类型数据
class intTuple(tuple):

    # 该方法创建类的实例对象，就是__init__中的self
    def __new__(cls, iterable):
        g = (x for x in iterable if isinstance(x, int) and x > 0)
        return super(intTuple, cls).__new__(cls, g)
        pass

    def __init__(self, iterable):
        super().__init__() #python3
        # super().__init__(iterable) python2 中需要这一句
    pass


## test

test = intTuple([-1, "a", "b", 3,[3,45,123], 10, -100])
print(test)
