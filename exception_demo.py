def demo1():
    try:
        raise IndexError
    except IndexError:
        print("index error")
    finally:
        print("finally")
    print("continue")


def demo2():
    try:
        raise IndexError
    finally:
        print("finally")
    print("continue")


if __name__ == "__main__":
    demo1()
