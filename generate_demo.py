def gen_squares(num):
    for x in range(num):
        yield x ** 2
    pass


# 返回值的类型为generator
g_demo = gen_squares(3)

print(type(g_demo))  # <class 'generator'>
i_demo = iter(g_demo)

print(type(i_demo))  # <class 'generator'>
print(i_demo is g_demo)  # True


# generator的迭代对象也是其本身，多以generator对象即是迭代器，又是可迭代的对象

def printDemo():
    print(iter(g_demo))
    print(next(g_demo))
    print(next(g_demo))
    print(next(g_demo))
    print(next(g_demo))


def gen_squares2(num):
    for x in range(num):
        yield x ** 2
        print('x={}'.format(x))


def printDemo2():
    for i in gen_squares2(4):
        print('x ** 2={}'.format(i))
        print('---' * 3)


printDemo2()


def gener():
    # 将列表解析式的[]换成()就变成了生成器表达式
    # 生产器表达式返回一个生成器对象，而不是一次性生成整个列表
    gen = (x ** 2 for x in range(5))
    print(type(gen))
