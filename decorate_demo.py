import random
from functools import wraps

"""
装饰器函数
"""


def decorate(fun):
    def wrap(*args, **kwargs):
        print("原函数执行两遍")
        fun(*args, **kwargs)
        fun(*args, **kwargs)

    return wrap;


@decorate
def demo():
    print("我们不一样{},{}")


demo()

"""
缓存内容
"""
cache = {}


def cache_decorate(fun):
    @wraps(fun)
    def _decorate(*args, **kwargs):
        key = '{}_{}_{}'.format(fun.__name__, args, kwargs);
        if key not in cache:
            cache[key] = fun(*args, **kwargs)
        return cache[key]

    return _decorate


@cache_decorate
def get_val_cache(key):
    return "这是一个缓存函数_{}".format(key)
    pass


print(get_val_cache("fsh"))
print(get_val_cache("love"))
print(get_val_cache("fsh"))


def singleton(cls):
    _instances = {}
    @wraps(cls)
    def wrapper(*args, **kwargs):
        key = "{}_{}_{}".format(cls.__name__, args, kwargs)
        if key not in _instances:
            _instances[key] = cls(*args, **kwargs)
        return _instances[key]
    return wrapper

@singleton
class Foo(object):
    def __init__(self, a=0):
        self.a = a
f1 = Foo(1)
f11 = Foo(1)
f2 = Foo(2)

print(f1 is f11)
print(f1 == f11)
print("a in f1 is ", f1.a)
print("a in f11 is ", f11.a)
print("a in f2 is ", f2.a)
