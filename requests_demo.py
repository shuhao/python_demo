import requests
from PIL import Image
from io import BytesIO

img_url = "http://bpic.588ku.com/element_origin_min_pic/16/12/04/023fdf76046fd281170b30256876ad76.jpg"
zip_url = "https://github.com/TheAlgorithms/Java/archive/master.zip"
postURL = "https://api.github.com/some/endpoint"


def demo(url):
    response = requests.get(url)
    print(response.status_code)


def downImg(url):
    response = requests.get(url)
    img_byte = response.content
    # i = Image.open(BytesIO(img_byte))
    file = open("down_img.jpg", "wb")
    file.write(img_byte)
    file.close()

def rawDemo(url):
    response = requests.get(url, stream=True)
    response.raw

def saveContentDemo(url):
    response = requests.get(url, stream=True)
    with open("raw_down_zip.zip", "wb") as file:
        for chunk in response.iter_content(chunk_size=128):
            file.write(chunk)


def postDataDemo(url):
    datas = {'some': 'data'}
    response = requests.post(url, data=datas)
    print(response.text)
    print(response.cookies)


if __name__ == "__main__":
    postDataDemo(postURL)
