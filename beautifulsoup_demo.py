from bs4 import BeautifulSoup
import requests

url = "http://sz.58.com/pinpaigongyu/pn/1/?minprice=2000_4000"


def demo():
    response = requests.get(url)
    soup = BeautifulSoup(response.text)
    class_icon = soup.find_all(attrs={"class": "icon"})
    print(type(class_icon))

    i = 0
    for icon in class_icon:
        i += 1
        print(icon.string)
    print(i)

    class_icon = soup.find_all("i", class_="icon")
    i = 0
    for icon in class_icon:
        i += 1
        print(icon.string)
    print(i)

    a_text = soup.find_all("a", text="地铁")
    for a in a_text:
        print(a)

    a_text = soup.find("a", text="地铁")
    print(a_text)


def demo2():
    soup = BeautifulSoup("<b></b>")
    tag_b = soup.b
    tag_b.name = "p"
    tag_b["class"] = "tip"
    tag_b["id"] = "msg"
    tag_b.string = "new msg"

    print(tag_b)

    tag_b.append("append msg")
    print(tag_b)

    tag_b.append("<a></a>")
    print(tag_b)

if __name__ == "__main__":
    demo2()
