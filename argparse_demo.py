import argparse

parser = argparse.ArgumentParser(description="argparse使用demo")
parser.add_argument("name", help="姓名")
parser.add_argument("age", type=int, help = "年龄")
parser.add_argument("-i", "--interest", default="basketball", help="兴趣爱好")  # 爱好
parser.add_argument("--flag", action="store_true")
parser.add_argument("--gender", choices=["man", "female"], default="man", required=True, help="性别")

#互斥参数
exclusive_group = parser.add_mutually_exclusive_group()
exclusive_group.add_argument("-l","--left", action="store_true")
exclusive_group.add_argument("-r","--right", action="store_true")

# 同时出现参数
argument_group = parser.add_argument_group()
argument_group.add_argument("--user", help="用户名")
argument_group.add_argument("--password", help="密码")

args = parser.parse_args()

if args.interest:
    print("name is %s, age is %s, interest is %s" % (args.name, args.age, args.interest))
else:
    print("name is %s, age is %s" % (args.name, args.age))

flag = args.flag

if flag:
    print("输入了flag！")
    pass
else:
    print("参数中没有flag参数！")

if args.gender:
    print("性别是："+args.gender)

if args.left:
    print("输入left!")
if args.right:
    print("输入right!")

print("用户名为：%s, 密码为：%s！" % (args.user, args.password))