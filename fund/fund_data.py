import requests

"""
数据来源天天基金
"""
fund_code = "000711"  # 基金代码
# 净值估算
jzgs_url = "https://fundmobapi.eastmoney.com/FundMApi/FundVarietieValuationDetail.ashx?FCODE={}&deviceid={}&plat={}&product={}&version={}"
# 历史净值
his_jz_url = "https://fundmobapi.eastmoney.com//FundMApi/FundNetDiagram.ashx?FCODE={}&pageSize={}&deviceid={}&plat={}&product={}&version={}"


def format_url(url, **kwargs):
    url = url.replace("FCODE={}", "FCODE={}".format(kwargs["FCODE"]))
    url = url.replace("deviceid={}", "deviceid={}".format(kwargs["deviceid"]))
    url = url.replace("plat={}", "plat={}".format(kwargs["plat"]))
    url = url.replace("product={}", "product={}".format(kwargs["product"]))
    url = url.replace("version={}", "version={}".format(kwargs["version"]))
    if ("pageSize={}" in url):
        url = url.replace("pageSize={}", "pageSize={}".format(kwargs["pageSize"]))
    return url
    pass


def get_data(url):
    # print(url)
    response = requests.get(url)
    jsondata = response.json()
    return jsondata
    pass


def get_curr_jz(**kwargs):
    """获取当前净值"""
    curr_jz_url = format_url(jzgs_url, **kwargs)
    jz_datas_temp = get_data(curr_jz_url)
    print(jz_datas_temp)
    curr_jz_data = jz_datas_temp["Expansion"]["GZ"]
    return curr_jz_data
    pass


def get_his_data(**kwargs):
    """获取历史净值"""
    his_jz_url_ = format_url(his_jz_url, **kwargs)
    his_jz_data_temp = get_data(his_jz_url_)
    his_jz_data_temp = his_jz_data_temp["Datas"]
    his_jz_datas = []
    for data in his_jz_data_temp:
        his_jz_datas.append(data["DWJZ"])
        pass
    return his_jz_datas


if (__name__ == "__main__"):
    params = {"FCODE": "000711", "deviceid": "F18916B5-9D5D-427A-B601-A10C7D0A53E9", "plat": "Iphone",
              "product": "EFund", "version": "5.2.0", "pageSize": "30"}
    datas = get_his_data(**params)
    print(datas)

    curr_jz = get_curr_jz(**params)
    print(curr_jz)
