import argparse


# 获取url参数
parser = argparse.ArgumentParser()
parser.add_argument("--fundCodes", reqired=True, help="基金编码，多个以逗号隔开")
parser.add_argument("--deviceid", help="设备编号", default="F18916B5-9D5D-427A-B601-A10C7D0A53E9")
parser.add_argument("--plat", help="设备平台", default="Iphone")
parser.add_argument("--product", help="接口平台", default="EFund")
parser.add_argument("--version", help="接口版本", default="5.2.0")
parser.add_argument("--hisValNum", help="查询历史净值的条数，默认为30条", default=30)

args = parser.parse_args()
fundCodes = args.funCodes
deviceId = args.deviceid
plat = args.plat
product = args.product
version = args.version
hisValNum = args.hisValNum

his_values = []  # 历史净值
his_low_values = []  # 历史净值中最低的10个值

curr_values = []  # 当前净值估算


def get_low_values(his_values, num):
    """计算历史净值中最低的几个值"""
    his_num = len(his_values)
    his_values.sort()
    if (num > his_num):
        return his_values
        pass
    his_low_values_temp = his_values[:num]
    return his_low_values_temp
    pass



def curr_in_his_low_value(curr_value):
    """
    判断当前净值是否可以购买
    1.在最低历史净值之间，2.小于最低历史净值， 3. 对于每个历史净值小于其对应百分比
    """
    v_len = len(his_low_values)
    begin = his_low_values[0]
    end = - his_low_values[v_len - 1]
    if (curr_value < begin):
        return True
    elif (curr_value > begin and curr_value < end):
        return True
    return False
    pass
