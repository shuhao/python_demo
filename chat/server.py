import asynchat
import asyncore

PORT = 6666


class EndSession(Exception):
    pass


"""
聊天服务器
"""


class ChatServer(asynchat.dispatcher):
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        # 创建socket
        self.create_socket()
        # 设置socket为可重用
        self.set_reuse_addr()
        # 监听端口
        self.bind(("", PORT))
        self.listen(5)
        self.users = {}
        self.main_room = ChatRoom(self)

    def handle_accept(self):
        conn, addr = self.accept()
        ChatSession(self, conn)


class ChatSession(asynchat.async_chat):
    """负责和客户端通信"""

    def __init__(self, server, socket):
        asynchat.async_chat.__init__(self, socket)
        self.server = server
        self.set_terminator(b'\n')
        self.data = []
        self.name = None
        self.enter(LoginRoom(server))

    def enter(self, room):
        # 从当前房间移除自身，然后添加到指定房间
        try:
            cur = self.room
        except AttributeError:
            pass
        else:
            cur.remove(self)
        self.room = room
        room.add(self)

    def collect_incoming_data(self, data):
        #接收客户端的数据
        self.data.append(data.decode("utf-8"))

    def found_terminator(self):
        #当客户端的一条数据结束时处理
        line =''.join(self.data)
        self.data=[]
        try:
            self.room.handle(self,line.encode("utf-8"))
        #退出聊天室的处理
        except EndSession:
            self.handle_close()

    def handle_close(self):
        #当session关闭时，将进入LogoutRoom
        asynchat.async_chat.handle_close(self)
        self.enter(LogoutRoom(self.server))



