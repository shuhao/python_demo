import itchat
import re
# 分词
import jieba
# wordcloud词云
import matplotlib.pyplot as plt
from wordcloud import WordCloud, ImageColorGenerator
import PIL.Image as Image
import os

import numpy as np

# 好友个性签名词云 生产图片

# 登录
itchat.auto_login(hotReload=True)

signatures = []


# 获取好友列表
def getSignatures():
    friends = itchat.get_friends(update=True)[0:]
    for i in friends:
        signature = i["Signature"]
        signature = filterSignature(signature)
        signatures.append(signature)
        print(signature)


def getNickName():
    friends = itchat.get_friends(update=True)[0:]
    nickNames = []
    for i in friends:
        nickNames.append(i["NickName"])
    return nickNames


# 过滤签名中的非文字
def filterSignature(signature):
    signature = signature.strip().replace("span", "").replace("class", "").replace("emoji", "")
    # 过滤表情
    rep = re.compile("1f\d.+")
    signature = rep.sub("", signature)
    return signature
    pass


# 获取所有的分词
def jiebaSignature(text):
    wordlist_jieba = jieba.cut(text, cut_all=True)
    wl_space_split = " ".join(wordlist_jieba)
    return wl_space_split
    pass


# 将获取的文字展示成一张图片
def showSignatureImg(text):
    my_wordcloud = WordCloud(background_color="white", max_words=2000, max_font_size=40, random_state=42,
                             font_path="C:\Windows\Fonts\STXINGKA.TTF").generate(text)
    plt.imshow(my_wordcloud)
    plt.axis("off")
    plt.show()


# 将文本生产给定图片样式
def showWordsUseImg(text):
    d = os.path.dirname(__file__)
    imgFile = Image.open(os.path.join(d, "wechat.jpg"))
    alice_coloring = np.array(imgFile)
    my_wordcloud = WordCloud(background_color="white", max_words=2000, max_font_size=20, random_state=42,
                             font_path="C:\Windows\Fonts\BOD_PSTC.TTF", width=500, height=200).generate(text)
    image_colors = ImageColorGenerator(alice_coloring)
    plt.imshow(my_wordcloud.recolor(color_func=image_colors))
    plt.imshow(my_wordcloud)
    plt.axis("off")
    plt.show()

    # 保存图片，发送手机
    my_wordcloud.to_file(os.path.join(d, "wechat_cloud.png"))
    itchat.send_image("wechat_cloud.png", 'filehelper')
    pass


# getSignatures()

# all_signature = jiebaSignature("".join(signatures))

# showSignatureImg(all_signature)


nikiNames = getNickName()
showWordsUseImg("".join(nikiNames))
