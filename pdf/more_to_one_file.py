#!/usr/bin/env Python
# coding=utf-8
import os, glob
import docx


"""
将之前pdf转换的多个word文档拼接成一个文档。
"""
for i in range(488, 2404, 4):
    docpath = "img-{}.docx".format(i)


def loadAllFile(path):
    files = os.listdir(r"E:\test\pdf")
    d = os.path.dirname(__file__)
    out = os.path.join(d, "all.docx")
    with open(out, 'wb') as outfile:
        for file in files:
            with open(file) as temp_file:
                outfile.write(temp_file.read())


def loadAllFile2():
    path = os.path.dirname(__file__)
    path = path + "/*.docx"
    files = glob.glob(path)
    list = range(488, 2404, 4)
    d = os.path.dirname(__file__)
    doc = docx.Document()
    for index in list:

        filePath = os.path.join(d, "img-{}.docx".format(index))
        file = docx.Document(filePath)
        for para in file.paragraphs:
            # para.text 读取word文档中一段的文本内容
            doc.add_paragraph(para.text)
        # with open(filePath, 'r') as file:
        #     for line in file:
        #         print(type(line))
        #         print(line)
        #         line_str = bytes.decode(line)
        #         print(type(line_str))
        #         print(line_str)
        #         doc.add_paragraph(line_str)
        pass
    doc.save("all_word.docx")


if (__name__ == "__main__"):
    loadAllFile2()
    pass
