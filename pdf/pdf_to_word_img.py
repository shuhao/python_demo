import os, re
import fitz
import argparse
import pytesseract
from PIL import Image
import docx

# pdf内容转换到word，pdf内容为图片

parser = argparse.ArgumentParser()
parser.add_argument("--filePath", help="需要转换的绝对文件路径")
parser.add_argument("--outWordPath", help="word文档的输出目录")
args = parser.parse_args()
filePath = args.filePath
outWordPath = args.outWordPath

d = os.path.dirname(__file__)
if (filePath == None):
    filePath = os.path.join(d, "demo.pdf")
if (outWordPath == None):
    outWordPath = os.path.join(d, "word.docx")

checkXO = r"/Type(?= */XObject)"  # finds "/Type/XObject"
checkIM = r"/Subtype(?= */Image)"  # finds "/Subtype/Image"


def findImg():
    print(filePath)
    doc = fitz.open(filePath)
    imgcount = 0
    lenXREF = doc._getXrefLength()
    print("file: %s, pages: %s, objects: %s" % (filePath, len(doc), lenXREF - 1))
    outWord = docx.Document(outWordPath)
    for i in range(1, lenXREF):  # scan through all objects
        text = doc._getObjectString(i)  # string defining the object
        isXObject = re.search(checkXO, text)  # tests for XObject
        isImage = re.search(checkIM, text)  # tests for Image
        if not isXObject or not isImage:  # not an image object if not both True
            continue
        imgcount += 1
        pix = fitz.Pixmap(doc, i)  # make pixmap from image
        if pix.n < 5:  # can be saved as PNG
            # print(type(pix))
            # pix_png = pix.getPNGData()
            # print(type(pix_png))
            # img = Image.open("img-488.png")
            # print(type(img))
            # img_2 = Image.eval(pix_png)
            # print(type(img_2))
            # text = pytesseract.image_to_string(pix_png, lang='chi_sim')
            addTextToOutWord(pix, outWord, i)
            pass
        else:  # must convert the CMYK first
            pix0 = fitz.Pixmap(fitz.csRGB, pix)
            # print(type(pix0))
            addTextToOutWord(pix, outWord, i)
            # pix0.writePNG("img-%s.png" % (i,))
            pix0 = None  # free Pixmap resources
        pix = None  # free Pixmap resources





# 读取pdf中解析出来的图片，写入到最后的word中
def addTextToOutWord(pix, outWord, i):
    pix.writePNG("img-%s.png" % (i,))
    text = readTextFromImg("img-%s.png" % (i,))
    outWord.add_paragraph(text);

def readTextFromImg(img_path):
    text = pytesseract.image_to_string(Image.open(img_path), lang='chi_sim')
    return text

if (__name__ == "__main__"):
    findImg()
