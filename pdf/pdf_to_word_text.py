from pdfminer.pdfparser import PDFParser, PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter, PDFTextExtractionNotAllowed
from pdfminer.layout import LAParams
from pdfminer.converter import PDFPageAggregator
import docx
import argparse
import os, time, re, sys


parse = argparse.ArgumentParser()
parse.add_argument("--filePath", help="需要转换的绝对文件路径")
parse.add_argument("--outPath", help="输出的word文档路径")

args = parse.parse_args()
filePath = args.filePath
outWordPath = parse.outPath


d = filePath = os.path.dirname(__file__)
if (filePath == None):
    filePath = os.path.join(d, "demo.pdf")

# 输出的word文档
if (outWordPath == None):
    filePath = os.path.join(d, "demo.pdf")


def parseFDF():
    with open(filePath, 'rb') as fn:
        print("pdf路径：{}".format(filePath))
        pdfParser = PDFParser(fn)
        doc = PDFDocument()
        pdfParser.set_document(doc)
        doc.set_parser(pdfParser)
        doc.initialize("")
        if not doc.is_extractable:
            raise PDFTextExtractionNotAllowed
        else:
            resource = PDFResourceManager()
            laparams = LAParams()
            device = PDFPageAggregator(resource, laparams=laparams)
            interpreter = PDFPageInterpreter(resource, device)
            # 内存中的doc
            content = docx.Document()
            for page in doc.get_pages():
                interpreter.process_page(page)
                layout = device.get_result()

                for out in layout:
                    if hasattr(out, "get_text"):
                        text = out.get_text()
                        print(text)
                        content.add_paragraph(text)
            content.save(outWordPath)
        pass
    pass





if (__name__ == "__main__"):
    parseFDF()
