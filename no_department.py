# 处理有orgId没有department的数据
# 调用方式
# python no_department.py --host 192.168.22.111 -p 27017 --eid 6823672
from pymongo import MongoClient
import argparse

# mongo库连接信息
parser = argparse.ArgumentParser()
parser.add_argument("--host", required=True, default="localhost", help="mongo库连接地址，必选项")

parser.add_argument('--port', type=int, default=27017, help="mongo端口号，默认为27017")
parser.add_argument("-u", "--user", help="mongo连接库的用户名")
parser.add_argument("-p", "--password", help="mongo连接库的密码")
parser.add_argument('--eid', required=True, help="需要处理的工作圈eid，必选项")

args = parser.parse_args()
host = args.host
port = args.port
user = args.user
password = args.password
eid = args.eid

# user = None
# password = None
# host = "192.168.22.111"
# port = 27017
# eid = "6823672"
# Mongo连接信息
if user and password:
    # 带用户名和密码的连接方式
    url = "mongodb://" + user + ":" + password + "@" + host + ":" + port + "/"
    client = MongoClient(url)
else:
    client = MongoClient(host, port)

ossDev = client["ossDev"]
c_userNetwork = ossDev["T_UserNetwork"]


# 查询对应的工作圈中orgId有值，而department没有值的记录
# 返回对应脏数据的cursor
def findDirtyData(eid):
    # dirtyData = c_userNetwork.find({"eid": eid, "department": "", "orgId": {"$ne": " "}})
    datas = c_userNetwork.find({"eid": eid})
    # dirtyDatas = []
    # if datas and datas.count() > 0:
    #     for data in datas:
    #         if "department" in data and "orgId" in data:
    #             department = data["department"]
    #             orgId = data["orgId"]
    #             if department == "" and orgId != "":
    #                 dirtyDatas.append(data)
    return datas


# 查询orgId对应的Department字段值
def findDepartment(orgId):
    orgDepartment = c_userNetwork.find_one({"eid": eid, "orgId": orgId, "department": {"$ne": ""}})
    # orgDepartment = c_userNetwork.find_one({"eid": eid})
    return orgDepartment["department"]
    pass


# 处理脏数据，将department数据加上
def dealDirtyData(dirtyDatas):
    if dirtyDatas:
        for dirtyData in dirtyDatas:
            if "department" in dirtyData and "orgId" in dirtyData:
                department = dirtyData["department"]
                orgId = dirtyData["orgId"]
                if department == "" and orgId != "":
                    print("脏数据为：" + str(dirtyData))
                    real_department = findDepartment(orgId)
                    if real_department:
                        _id = dirtyData["_id"]
                        c_userNetwork.update_one({"_id": _id}, {"$set": {"department": real_department}})


if __name__ == "__main__":
    dirtyDatas = findDirtyData(eid)
    if dirtyDatas:
        dealDirtyData(dirtyDatas)

    else:
        print("工作圈：%s没有查询到符合条件的脏数据！" % (eid))
